# Binary Search Progression

## Language Mechanics

- Accessing the middle element of a slice
	- Edit the file [`middle_index.go`](../mechanics/middle_index.go)
	- Run the tests via `go test -run=TestMiddleElement
	  gitlab.com/pokstad1/goprogs/mechanics`
- Splitting a slice around an element
	- Edit the file [`split_slice.go`](../mechanics/split_slice.go)
	- Run the test via `go test -run=TestSplitSlice
	  gitlab.com/pokstad1/goprogs/mechanics`
- Recursive split slice
	- Edit the file [`recursive_split.go`](../mechanics/recursive_split.go)
	- Run the test via `go test -run=TestRecursiveSplit
	  gitlab.com/pokstad1/goprogs/mechanics`

## Linear Search

The naive solution that preludes binary search is linear search. For linear
search, we want to start at the first element and check each element until we
find the solution.

Update [`LinearSearch`](binary_search.go) function to make the test inside of
[`binary_search_test.go`](binary_search_test.go) pass using the Linear Search
algorithm. To run the test you can run `go test -v -run
'TestSearch/Linear_Search'  gitlab.com/pokstad1/goprogs/algorithms`

### Discussion

- What is the big-O time complexity? Space complexity?
- When is linear search appropriate?

### Reference

- [Linear vs Binary Search](https://www.geeksforgeeks.org/linear-search-vs-binary-search/)

## Binary Search

Binary search improves on linear search by reducing the problem space after
each successful check.

Update [`BinarySearch`](binary_search.go) function to make the test inside of
[`binary_search_test.go`](binary_search_test.go) pass using the Linear Search
algorithm. To run the test you can run `go test -v -run 'TestSearch/Binary_Search'  gitlab.com/pokstad1/goprogs/algorithms`

### Discussion

- Is your solution iterative or recursive?
- What is the big-O time complexity? Space complexity?

### Reference

- [Binary Search @ Khan Academy](https://www.khanacademy.org/computing/computer-science/algorithms/binary-search/a/binary-search)

## Ternary Search

Binary search splits the problem set into two halves and eliminates the
uninteresting half. Ternary search splits the problem into thirds and
eliminates the two uninteresting thirds.

Update [`TernarySearch`](binary_search.go) function to make the test inside of
[`binary_search_test.go`](binary_search_test.go) pass using the Linear Search
algorithm. To run the test you can run `go test -v -run 'TestSearch/Ternary_Search'  gitlab.com/pokstad1/goprogs/algorithms`

### Discussion

- Is ternary search an improvement on binary search? Why or why not?

### Reference

- [Binary vs Ternary Search](https://www.geeksforgeeks.org/binary-search-preferred-ternary-search/)

## Meta Binary Search

Meta Binary Search is an alternative to traditional binary search. The index
is incrementally built via bit-wise operations.

Update [`MetaBinarySearch`](binary_search.go) function to make the test inside of
[`binary_search_test.go`](binary_search_test.go) pass using the Linear Search
algorithm. To run the test you can run `go test -v -run 'TestSearch/Meta_Binary_Search'  gitlab.com/pokstad1/goprogs/algorithms`

### Discussion

- Why would it be advantageous to incrementally build the index?

### Reference

- [Meta Binary Search | One Sided Binary Search](https://www.geeksforgeeks.org/meta-binary-search-one-sided-binary-search/)

## Further Reading

- [Go's `"sort".Search`](https://golang.org/pkg/sort/#Search) in the standard library.
- [Binary Search Problems](https://medium.com/@codingfreak/binary-search-practice-problems-4c856cd9f26c)

